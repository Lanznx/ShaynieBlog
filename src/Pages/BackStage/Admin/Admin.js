import React from "react"
import Grid from "@mui/material/Grid"
import {
  Typography,
} from "@mui/material"
import AdminAction from "./components/AdminAction"
import Swal from "sweetalert2"
import { checkSignIn } from "../checkSignIn"

export default function AdminPage() {
  React.useEffect(() =>{
    refresh()
  }, [])

  const refresh = async () => {
    const isSignIn = await checkSignIn()
    if (isSignIn === -1) {
      const user = localStorage.getItem("user")
      const displayName = JSON.parse(user).displayName
      await Swal.fire(
        `${displayName}，找到這個頁面真有你的\n但你不是被授權的用戶，請不要亂來`,
        "", "error",
      )
      window.location.href = "/"
    } else if (!isSignIn) {
      await Swal.fire("您尚未登入", "", "error")
      window.location.href = "/admin/signin"
    }
  }  
  return (
    <Grid container spacing={3}>
      <Grid item xs={1} lg={4} />
      <Grid container xs={10} lg={4} >
        <Grid item xs={12} lg={12}>
          <Typography variant="h9">哈囉，這裡是後台 </Typography>
        </Grid>
        <Grid item xs={12} lg={12}>
          <AdminAction
            actionName="寫點東西"
            url="/admin/editor"
            imgUrl={"https://firebasestorage.googleapis.com/" +
             "v0/b/shaynie-blog.appspot.com/o/article%2F%E5%AF%AB%E"+
             "6%96%87%E7%AB%A0?alt=media&token"+
             "=ae2b29f0-837a-49cc-8ef5-d3687694a50a"}
          />
        </Grid>
        <Grid item xs={12} lg={12}>
          <AdminAction
            actionName="整理文章"
            url="/admin/list"
            imgUrl={
              "https://firebasestorage.googleapis.com/v0/b/shaynie-blog."+
              "appspot.com/o/article%2F%E6%95%B4%E7%90%86%E5%88%86%E9%A1%9E"+
              "?alt=media&token=bd875402-1d8b-4da4-819d-7170f771a1b7"
            }
          />
        </Grid>

        <Grid item xs={12} lg={12}>
        </Grid>
      </Grid>
      <Grid item xs={1} lg={4} />
    </Grid>
  )
}
