import React from "react"
import {
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  Typography,
  Grid,
} from "@mui/material"
import PropTypes from "prop-types"

AdminAction.propTypes = {
  actionName: PropTypes.string,
  url: PropTypes.string,
  imgUrl: PropTypes.string,
}

export default function AdminAction(props) {
  const { actionName, url, imgUrl } = props
  return (
    <Card sx={{ width: "full", spacing: 1, margin: 1 }}>
      <CardActionArea onClick={() => {
        window.location.href = url
      }}>
        <Grid container>
          <Grid item xs={3} >
            <CardMedia
              sx={{ padding: 1 }}
              component="img"
              width="100%"
              image={imgUrl} alt="random"
            />
          </Grid>
          <Grid item xs={9} >
            <CardContent>
              <Typography gutterBottom variant="h5" component="div">
                {actionName}
              </Typography>
            </CardContent>
          </Grid>
        </Grid>
      </CardActionArea>
    </Card>
  )
}