const checkSignIn = () => {
  try {
    const user = localStorage.getItem("user")
    if (user === null) {
      return false
    }
    const email = JSON.parse(user).email 
    const token = JSON.parse(user).stsTokenManager.accessToken 
    if (email === "" || token === "") {
      return false
    }
    if (email !== "andyleu100@gmail.com") {
      return -1
    }
    return true
  } catch (e) {
    console.log(e)
    return false
  }
}
export { checkSignIn }