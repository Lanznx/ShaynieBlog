import { getAuth, signInWithPopup, GoogleAuthProvider } from "firebase/auth"

const handleLoginGoogle = async () => {
  try {
    const provider = new GoogleAuthProvider()
    const auth = getAuth()
    const result = await signInWithPopup(auth, provider)
    const user = result.user
    window.localStorage.setItem("user", JSON.stringify(user))
    window.location.href = "/admin"
  }
  catch (error) {
    console.log(error)
  }
}

export { handleLoginGoogle }