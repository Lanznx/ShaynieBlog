import React from "react"
import { green } from "@mui/material/colors"
import { Box, Button, CircularProgress, Grid } from "@mui/material"
import { handleLoginGoogle } from "./APIs"
import GoogleIcon from "../../../Assets/icons/google.png"
import { checkSignIn } from "../checkSignIn"
import Swal from "sweetalert2"

export default function SignIn() {
  const [loading, setLoading] = React.useState(false)

  const handleButtonClick = async () => {
    if (!loading) {
      setLoading(true)
    }
    await handleLoginGoogle()
    setLoading(false)
  }

  React.useEffect(async () => {
    if (loading) {
      setLoading(false)
    }
    const isSignIn = checkSignIn()
    if (isSignIn === -1) {
      const user = localStorage.getItem("user")
      const displayName = JSON.parse(user).displayName
      await Swal.fire(
        `${displayName}，找到這個頁面真有你的\n但你不是被授權的用戶，請不要亂來`,
        "", "error",
      )
      window.location.href = "/"
    } else if (isSignIn) {
      window.location.href = "/admin"
    }
  }, [])

  return (
    <Grid container spacing={3}>
      <Grid item xs={2} />
      <Grid item xs={8}>
        <Box sx={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
        >
          <h1>SignIn</h1>
          <Button onClick={handleButtonClick}
            variant="contained"
            color="primary"
            disabled={loading}
            sx={{
              width: "100px",
              height: "40px",
              display: "flex",
            }}
          >
            <img src={GoogleIcon} alt="google icon" style={{
              width: "20px",
              marginRight: "10px",
            }} />
            登入
          </Button>
          {loading && (
            <CircularProgress
              size={24}
              sx={{
                color: green[500],
                position: "absolute",
                top: "50%",
                left: "50%",
                marginTop: "-12px",
                marginLeft: "-12px",
              }}
            />
          )}
        </Box>
      </Grid>
      <Grid item xs={2} />
    </Grid>
  )

}