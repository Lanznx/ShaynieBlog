import React from "react"
import {
  Card,
  CardContent,
  CardMedia,
  Typography,
  Grid,
  IconButton,
  CircularProgress,
} from "@mui/material"
import VisibilityOffIcon from "@mui/icons-material/VisibilityOff"
import VisibilityIcon from "@mui/icons-material/Visibility"
import AddHomeIcon from "@mui/icons-material/AddHome"
import BookmarkRemoveIcon from "@mui/icons-material/BookmarkRemove"
import PropTypes from "prop-types"
import { setSelected, setVisible } from "../APIs"
import { green } from "@mui/material/colors"
import Swal from "sweetalert2"
AdminArticle.propTypes = {
  title: PropTypes.string,
  editUrl: PropTypes.string,
  imgUrl: PropTypes.string,
  preview: PropTypes.string,
  visible: PropTypes.bool,
  id: PropTypes.string,
  refresh: PropTypes.func,
  selected: PropTypes.bool,
}

export default function AdminArticle(props) {
  const { title, imgUrl, preview, visible, id, refresh, selected } = props
  const [visibleLoading, setVisibleLoading] = React.useState(false)
  const [selectedLoading, setSelectedLoading] = React.useState(false)


  const handleSelect = async (isSelected) => {
    setSelectedLoading(true)
    await setSelected(id, isSelected)
    if (isSelected) {
      Swal.fire({
        icon: "success",
        title: "成功",
        text: "已設為首頁文章",
      })
    }
    else {
      Swal.fire({
        icon: "success",
        title: "成功",
        text: "已取消首頁文章",
      })
    }
    refresh()
    setSelectedLoading(false)
  }

  return (
    <Card sx={{ width: "full", spacing: 1, margin: 1, direction: "row" }}>
      <Grid container>
        <Grid item xs={8} >
          <Grid container>
            <Grid item xs={4} >
              <CardMedia
                sx={{ padding: 1 }}
                component="img"
                height="100%"
                image={imgUrl} alt="random"
              />
            </Grid>
            <Grid item xs={8} >
              <CardContent>
                <Typography gutterBottom variant="h5" component="div">
                  {title}
                </Typography>
                <p>
                  {preview}...
                </p>
              </CardContent>
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={4} sx={{
          display: "flex",
          justifyContent: "flex-end",
          wrap: "flex-wrap",
        }}>
          <IconButton disabled={visible || visibleLoading}
            hidden={visible}
            onClick={async () => {
              setVisibleLoading(true)
              await setVisible(id, true)
              await refresh()
              Swal.fire("已發布", "", "success")
              setVisibleLoading(false)
            }}
          >
            {visibleLoading && (
              <CircularProgress
                size={24}
                sx={{
                  color: green[500],
                  position: "absolute",
                  top: "50%",
                  left: "50%",
                  marginTop: "-12px",
                  marginLeft: "-12px",
                }}
              />
            )}
            <VisibilityIcon />
          </IconButton>
          <IconButton disabled={!visible || visibleLoading}
            hidden={!visible}
            onClick={async () => {
              setVisibleLoading(true)
              await setVisible(id, false)
              await refresh()
              Swal.fire("已隱藏", "", "success")
              setVisibleLoading(false)
            }}
          >
            {visibleLoading && (
              <CircularProgress
                size={24}
                sx={{
                  color: green[500],
                  position: "absolute",
                  top: "50%",
                  left: "50%",
                  marginTop: "-12px",
                  marginLeft: "-12px",
                }}
              />
            )}
            <VisibilityOffIcon />
          </IconButton>
          <IconButton disabled={selected || selectedLoading}
            onClick={() => {
              handleSelect(true)
            }}
          >
            {selectedLoading && (
              <CircularProgress
                size={24}
                sx={{
                  color: green[500],
                  position: "absolute",
                  top: "50%",
                  left: "50%",
                  marginTop: "-12px",
                  marginLeft: "-12px",
                }} />
            )}
            <AddHomeIcon />
          </IconButton>
          <IconButton disabled={!selected || selectedLoading}
            onClick={() => {
              handleSelect(false)
            }}
          >
            {selectedLoading && (
              <CircularProgress
                size={24}
                sx={{
                  color: green[500],
                  position: "absolute",
                  top: "50%",
                  left: "50%",
                  marginTop: "-12px",
                  marginLeft: "-12px",
                }} />
            )}
            <BookmarkRemoveIcon />
          </IconButton>
        </Grid>
      </Grid>
    </Card>
  )
}