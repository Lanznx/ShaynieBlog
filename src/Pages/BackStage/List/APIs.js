import { collection, doc, getDocs, updateDoc } from "firebase/firestore"
import { db } from "../../../firebase/api"

const getArticleList = async () => {
  const articleList = []
  const path = collection(db, "article")
  const docRef = await getDocs(path)
  docRef.forEach(doc => {
    articleList.push({
      id: doc.id,
      ...doc.data(),
    })
  })
  return articleList
}

const setVisible = async (id, visible) => {
  const docRef = doc(db, "article", id)
  await updateDoc(docRef, {
    visible: visible,
  })
}

const setSelected = async (id, selected) => {
  const docRef = doc(db, "article", id)
  await updateDoc(docRef, {
    selected: selected,
  })
}


export { getArticleList, setVisible, setSelected }