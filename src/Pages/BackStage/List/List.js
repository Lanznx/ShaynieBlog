import React from "react"
import { Grid } from "@mui/material"
import AdminArticle from "./components/AdminArticle"
import { getArticleList } from "./APIs"


export default function List() {
  const [articleList, setArticleList] = React.useState([{
    title: "載入中...",
    editUrl: "",
    imgUrl: "https://firebasestorage.googleapis.com/v0/b/shaynie-"+
    "blog.appspot.com/o/article%2F%E4%B8%8A%E5%82%B3%E5%9C%96%E7"+
    "%89%87?alt=media&token=2c169b79-c802-470f-ab4b-80d6c23653b8",
  }])
  React.useEffect(() => {
    refresh()
  }, [])
  async function refresh() {
    const tempArticleList = await getArticleList()
    const sortedArticleList = quickSort(tempArticleList)
    setArticleList(sortedArticleList)
  }

  function quickSort(articleList) {
    if (articleList.length <= 1) {
      return articleList
    }
    const pivot = articleList[0]
    const left = []
    const right = []
    for (let i = 1; i < articleList.length; i++) {
      if (articleList[i].createTime > pivot.createTime) {
        left.push(articleList[i])
      } else {
        right.push(articleList[i])
      }
    }
    return quickSort(left).concat(pivot, quickSort(right))

  }

  return (
    <Grid container spacing={3}>
      <Grid item xs={.5} lg={3} />
      <Grid item xs={11} lg={6}>
        <h1>文章列表</h1>
        {
          articleList.map((article, index) => {
            return (
              <AdminArticle
                key={index}
                title={article.title}
                editUrl={`/admin/editor/${article.title}`}
                imgUrl={article.imgUrl}
                preview={article.preview}
                visible={article.visible}
                id={article.id}
                refresh={refresh}
                selected={article.selected}
              />
            )
          },
          )
        }

      </Grid>
      <Grid item xs={.5} lg={3} />

    </Grid>

  )
}