import React from "react"
import { Card, CardContent } from "@mui/material"
import PropTypes from "prop-types"

Preview.propTypes = {
  title: PropTypes.string,
  preview: PropTypes.string,
  image: PropTypes.object,
}

export default function Preview(props) {
  const [visible, setVisible] = React.useState(false)
  

  React.useEffect(() => {
    if(props.image.previewUrl !== undefined){
      setVisible(true)
    }
  }, [props.image])


  return (
    <>
      <h1>預覽</h1>
      <Card sx={{
        marginTop: 2,
        marginBottom: 2,
      }}>
        <CardContent sx={{ border: 1, borderColor: "grey.300" }}>
          <img
            src={
              props.image.previewUrl
            }
            hidden={!visible}
            alt="preview"
            style={{
              width: "100%",
            }} />
          <h2>{props.title}</h2>
          <div dangerouslySetInnerHTML={{ __html: props.preview }} />
        </CardContent>
      </Card>
    </>
  )
}