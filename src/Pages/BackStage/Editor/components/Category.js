import { FormHelperText, Grid, Input, MenuItem, Select } from "@mui/material"
import React from "react"
import PropTypes from "prop-types"
Category.propTypes = {
  categories: PropTypes.array,
  setNewCategory: PropTypes.func,
}

export default function Category(props) {
  const { categories, setNewCategory } = props
  const [canSelect, setCanSelect] = React.useState(false)
  React.useEffect(() => {
    if (categories.length > 0) {
      setCanSelect(true)
    }
  }, [categories])

  const handleChange = (event) => {
    setNewCategory({
      name: event.target.value,
      amount: 1,
    })
  }
  return (
    <Grid container
      sx={{
        display: "flex",
        justifyContent: "bottom",
      }}
    >
      <Grid item xs={6}>
        <FormHelperText sx={{
          mt: 2,
        }}>文章分類</FormHelperText>
        <Select labelId="demo-simple-select-label"
          id="demo-simple-select" disabled={!canSelect}
          placeholder="請選擇分類"
          sx={{
            width: "80%",
            height: "40px",
          }}
          onChange={handleChange}
        >
          {categories.map((category, index) => {
            return (
              <MenuItem key={index} value={category.name}>
                {`${category.name}`}
              </MenuItem>
            )
          })}
        </Select>
      </Grid>
      <Grid item xs={6}>
        <FormHelperText sx={{
          mt: 2,
        }}>或是建立新的分類</FormHelperText>
        <Input
          placeholder="請輸入文章分類"
          onChange={handleChange}
          sx={{
            height: "40px",
            width: "80%", mb: 2, border: 1, borderColor: "grey.100",
            borderRadius: 1, p: 1,
          }}
        />
      </Grid>
    </Grid>
  )
}