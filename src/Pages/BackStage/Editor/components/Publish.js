import * as React from "react"
import Box from "@mui/material/Box"
import CircularProgress from "@mui/material/CircularProgress"
import { green } from "@mui/material/colors"
import Button from "@mui/material/Button"
import { saveAndPublish } from "../APIs"
import Swal from "sweetalert2"
import PropTypes from "prop-types"

PublishButton.propTypes = {
  article: PropTypes.string,
  title: PropTypes.string,
  image: PropTypes.object,
  newCategory: PropTypes.object,
  categories: PropTypes.array,
}

export default function PublishButton(props) {
  const { article, title, image, newCategory, categories } = props
  const [loading, setLoading] = React.useState(false)
  const [canPublish, setCanPublish] = React.useState(false)
  React.useEffect(() => {
    if (checkArticle()) {
      setCanPublish(true)
    }
  }, [article, title, image])

  const handleButtonClick = async () => {
    if (!checkArticle()) {
      Swal.fire("請輸入標題和內容", "", "error")
      return
    }

    if (!loading) {
      setLoading(true)
      const isContinue = await Swal.fire({
        title: "確定要儲存並發布，發布之後就不能修改喔！",
        showCancelButton: true,
        confirmButtonText: "確定",
      })

      if (!isContinue.isConfirmed) {
        setLoading(false)
        return
      }
      const preview = article.slice(0,80).replace(/<[^>]+>/g, "")
      const isPublish = await saveAndPublish(
        article,
        title,
        image,
        preview,
        newCategory,
        categories,
      )
      if (isPublish) {
        await Swal.fire("已儲存並發布", "", "success")
        window.location.href = "/admin/list"
        setLoading(false)
      } else {
        Swal.fire("儲存失敗，請跟呂安說一下", "", "error")
        setLoading(false)
        return
      }
    }
  }

  const checkArticle = () => {
    if (article === "") {
      return false
    } else if (title === "") {
      return false
    } else if (!image) {
      return false
    }
    return true
  }

  return (
    <Box sx={{ mt: 2, display: "flex", alignItems: "center" }}>
      <Box sx={{ position: "relative" }}>
        <Button
          variant="contained"
          disabled={loading || !canPublish}
          onClick={handleButtonClick}
        >
          儲存並發布
        </Button>
        {loading && (
          <CircularProgress
            size={24}
            sx={{
              color: green[500],
              position: "absolute",
              top: "50%",
              left: "50%",
              marginTop: "-12px",
              marginLeft: "-12px",
            }}
          />
        )}
      </Box>
    </Box>
  )
}
