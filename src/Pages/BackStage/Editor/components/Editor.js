import React from "react"
import ReactQuill from "react-quill"
import "react-quill/dist/quill.snow.css"
import PropTypes from "prop-types"




export default class Editor extends React.Component {
  constructor(props) {
    super(props)
    this.quillRef = null // Quill instance
    this.reactQuillRef = null // ReactQuill component
  }


  modules = {
    toolbar: [
      [{ "header": [1, 2, false] }],
      [{ "color": [] }, { "background": [] }],
      ["bold", "italic", "underline", "strike", "blockquote"],
      [{ "list": "ordered" }, { "list": "bullet" }],
      ["link", 
        // "image",
      ],
    ],
  }

  componentDidMount() {
    this.attachQuillRefs()
  }

  componentDidUpdate() {
    this.attachQuillRefs()
  }

  attachQuillRefs = () => {
    if (typeof this.reactQuillRef.getEditor !== "function") return
    this.quillRef = this.reactQuillRef.getEditor()
  }


  render() {
    return (
      <div>
        <ReactQuill
          ref={(el) => {
            this.reactQuillRef = el
          }}
          theme={"snow"}
          modules={this.modules}
          value={this.props.article}
          onChange={this.props.setArticle}
        />
      </div>
    )
  }
}Editor.propTypes = {
  article: PropTypes.string,
  title: PropTypes.string,
  setArticle: PropTypes.func,
  setTitle: PropTypes.func,
}