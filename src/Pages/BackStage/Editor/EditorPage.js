import React from "react"
import Grid from "@mui/material/Grid"
import { Button, Input } from "@mui/material"

import Editor from "./components/Editor"
import Preview from "./components/Preview"
import PublishButton from "./components/Publish"
import { checkSignIn } from "../checkSignIn"
import Swal from "sweetalert2"
import Category from "./components/Category"
import { getCategories } from "./APIs"

export default function EditorPage() {
  const [image, setImage] = React.useState({})
  const [article, setArticle] = React.useState("")
  const [title, setTitle] = React.useState("")
  const [categories, setCategories] = React.useState([{
    name: "請選擇分類",
    amount: 0,
  }])
  const [newCategory, setNewCategory] = React.useState({
    name: "",
    amount: 1,
  })

  const handleUpload = (e) => {
    if (e.target.files.length === 0) return
    const img = e.target.files[0]
    setImage(img)
    img.previewUrl = URL.createObjectURL(img)
  }

  const refresh = async() => {
    const tempCategories = await getCategories()
    if(!tempCategories) return
    setCategories(tempCategories)
  }

  React.useEffect(() => {
    const isSignIn = checkSignIn()
    if (isSignIn === -1) {
      const user = localStorage.getItem("user")
      const displayName = JSON.parse(user).displayName
      Swal.fire(
        `${displayName}，找到這個頁面真有你的\n但你不是被授權的用戶，請不要亂來`,
        "", "error",
      )
      window.location.href = "/"
    } else if (!isSignIn) {
      window.location.href = "/admin/signin"
    }
    refresh()
  }, [])

  return (
    <Grid container spacing={3}>
      <Grid item lg={2} xs={.5} />
      <Grid item lg={8} xs={11}>

        <Preview preview={article} title={title} image={image} />
        <h1 style={{
          marginTop: "100px",
        }}>編輯文章</h1>
        <Input
          placeholder="請輸入標題"
          value={title}
          onChange={(e) => setTitle(e.target.value)}
          sx={{
            width: "100%", mb: 2, border: 1, borderColor: "grey.100",
            borderRadius: 1, p: 1,
          }}
        />
        <Button variant="contained" sx={{ mb: 2 }} component="label">
          <input
            onChange={handleUpload}
            type="file"
            hidden
          />
          上傳文章首圖
        </Button>

        <Editor article={article} setArticle={setArticle} />
        <Category categories={categories} setNewCategory={setNewCategory}/>
        <PublishButton 
          article={article}
          title={title} 
          image={image}
          newCategory={newCategory}
          categories={categories}
        />
      </Grid>
      <Grid item lg={2} xs={.5}/>
    </Grid >

  )
}

