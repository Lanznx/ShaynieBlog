import { addDoc, collection, doc, getDocs, updateDoc } from "firebase/firestore"
import {
  getDownloadURL,
  ref,
  uploadBytesResumable,
} from "firebase/storage"
import {
  storage,
  db,
} from "../../../firebase/api"


const saveAndPublish = async (
  article,
  title,
  image,
  preview,
  newCategory,
  categories,
) => {
  try {
    const imgUrl = await uploadArticleImage(image, title)
    addNewCategory(categories, newCategory)
    await addDoc(collection(db, "article"), {
      content: article,
      title: title,
      createTime: new Date(),
      imgUrl: imgUrl,
      preview: preview,
      category: newCategory.name,
      visible: true,
    })
    return true
  } catch (e) {
    console.log(e)
    return false
  }
}

const uploadArticleImage = async (image, title) => {
  const imageRef = await ref(storage, `article/${title}`)
  const uploadTask = await uploadBytesResumable(imageRef, image)
  const downloadURL = await getDownloadURL(uploadTask.ref)
  return downloadURL
}

const getCategories = async () => {
  const categories = []
  const docRef = await getDocs(collection(db, "category"))
  docRef.forEach((doc) => {
    categories.push({
      id: doc.id,
      ...doc.data(),
    })
  })
  return categories
}

const addNewCategory = async (categories, newCategory) => {
  try {
    const categoryInDB = categories.find(
      (category) => category.name === newCategory.name,
    )
    if (categoryInDB) {
      await updateDoc(doc(db, "category", categoryInDB.id), {
        amount: categoryInDB.amount + 1,
      })
      return false
    }
    await addDoc(collection(db, "category"), newCategory)
    return true
  } catch (e) {
    console.log(e)
    return false
  }
}




export { saveAndPublish, uploadArticleImage, getCategories }