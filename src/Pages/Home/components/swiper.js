import React from "react"
import { Navigation, Pagination, Scrollbar, A11y } from "swiper"
import { Swiper, SwiperSlide } from "swiper/react"
import "swiper/css"
import "swiper/css/navigation"
import "swiper/css/pagination"
import "swiper/css/scrollbar"
import "swiper/css/autoplay"
import { getSelectedArticleList } from "../APIs"
import { Card, CardActionArea } from "@mui/material"

export default function ArticleSwiper() {
  const [articleList, setArticleList] = React.useState([{
    title: "緩緩的 緩緩的",
    imgUrl: "https://firebasestorage.googleapis.com/v0/b/shaynie-"+
    "blog.appspot.com/o/article%2F%E4%B8%8A%E5%82%B3%E5%9C%96%E7"+
    "%89%87?alt=media&token=2c169b79-c802-470f-ab4b-80d6c23653b8",
  }])
  React.useEffect(() => {
    refresh()
  }, [])

  const refresh = async () => {
    const temp = await getSelectedArticleList()
    setArticleList(temp)
  }

  return (
    <Swiper
      modules={[Navigation, Pagination, Scrollbar, A11y]}
      slidesPerView={1}
      spaceBetween={800}
      loop={true}
      autoplay={{
        delay: 3000,
        disableOnInteraction: false,
      }}
      navigation
      pagination={{ clickable: true }}
      onSwiper={(swiper) => console.log(swiper)}
      onSlideChange={() => console.log("slide change")}
    >
      {
        articleList.map((article, index) => {
          return (
            <SwiperSlide key={index} style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}>
              <Card style={{
                height: 300,
                borderRadius: 10,
                overflow: "hidden",
              }}>
                <CardActionArea onClick={()=>{
                  window.location.href = `/article/${article.title}`
                }}>
                  <img src={article.imgUrl} style={{
                    height: 300,
                  }} alt="article" />
                </CardActionArea>
              </Card>
              <h1>{article.title}</h1>
            </SwiperSlide>
          )
        })
      }
    </Swiper>
  )
}