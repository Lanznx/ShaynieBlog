import React from "react"
import Grid from "@mui/material/Grid"
import ArticleSwiper from "./components/swiper"

export default function Home() {
  return (
    <Grid container spacing={3}>
      <Grid item xs={.5} lg={2} />
      <Grid item xs={11} lg={8}>
        <h1>Home</h1>
        <ArticleSwiper />
      </Grid>
      <Grid item xs={.5} lg={2}/>
    </Grid>
  )
}
