import { collection, getDocs } from "firebase/firestore"
import { db } from "../../firebase/api"

const getSelectedArticleList = async () => {
  const articleList = []
  const path = collection(db, "article")
  const docRef = await getDocs(path)

  docRef.forEach(doc => {
    if (doc.data().visible && doc.data().selected) {
      articleList.push(doc.data())
    }
  })
  return articleList
}

export { getSelectedArticleList }