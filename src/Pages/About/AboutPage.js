import smiling_sea from "../../Assets/Images/smiling_sea.jpg"
import React from "react"
import Grid from "@mui/material/Grid"
import { Typography } from "@mui/material"

export default function About() {
  return (
    <Grid container spacing={3}>
      <Grid item xs={.5} lg={2} />
      <Grid container xs={11} lg={8}
        direction="column"
        justifyContent="center"
        alignItems="center"

      >
        <h1>哈囉，請叫我 Shaynie ! </h1>
        <img src={smiling_sea} alt="smiling_sea" width="60%" />
        <Typography variant="h6" fontWeight="medium">哩賀，哇洗蔡紗</Typography>
      </Grid>
      <Grid item xs={.5} lg={2} />
    </Grid>
  )
}
