import * as React from "react"
import CssBaseline from "@mui/material/CssBaseline"
import Box from "@mui/material/Box"
import Typography from "@mui/material/Typography"
import Container from "@mui/material/Container"

export default function Footer() {
  return (
    // make the footer stick to the bottom
    <Box
      component="footer"
      sx={{
        bottom: 0,
        position: "sticky",
        py: 3,
        px: 2,
        mt: "100px",
        // bgcolor: "background.paper",
        textAlign: "center",
        // transparent
        bgcolor: "rgba(255, 255, 255, 0.5)",
      }}
    >
      <CssBaseline />
      <Container maxWidth="sm">
        <Typography variant="body2" color="secondary">
          {"Copyright © "}
            Shaynie Blog
          {" "}
          {new Date().getFullYear()}
          {"."}
        </Typography>
      </Container>
    </Box>
  )
}
