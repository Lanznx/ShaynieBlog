import React from "react"
import Grid from "@mui/material/Grid"
import { useParams } from "react-router-dom"
import Article from "./components/Article"

export default function ArticlePage() {
  // const id = props.match.params.id;
  // get the id from the url
  const { title } = useParams()
  return (
    <Grid container spacing={3}>
      <Grid item xs={1} lg={3}/>
      <Grid container xs={10.5} lg={6}>
        <Article title={title}/>
      </Grid>
      <Grid item xs={1} lg={3} />
    </Grid>
  )
}
