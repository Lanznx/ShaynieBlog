import { collection, getDocs } from "firebase/firestore"
import { db } from "../../firebase/api"

const getArticle = async (title) => {
  const path = collection(db, "article")
  const docRef = await getDocs(path)
  let article = null
  docRef.forEach(doc => {
    if (doc.data().title === title) {
      article =  doc.data()
    }
  })
  return article
}

export { getArticle }