import React from "react"
import {
  Card,
  CardMedia,
  CardContent,
} from "@mui/material"
import PropTypes from "prop-types"
import { getArticle } from "../APIs"
Article.propTypes = {
  title: PropTypes.string,
}

export default function Article(props) {
  const { title } = props
  const [article, setArticle] = React.useState({})
  const [imgUrl, setImgUrl] = React.useState(
    "https://firebasestorage.googleapis.com/v0/b/shaynie-"+
    "blog.appspot.com/o/article%2F%E4%B8%8A%E5%82%B3%E5%9C%96%E7"+
    "%89%87?alt=media&token=2c169b79-c802-470f-ab4b-80d6c23653b8",
  )
  React.useEffect(() => {
    refresh()
  }, [])

  const refresh = async() => {
    const tempArticle = await getArticle(title)
    setArticle(tempArticle)
    setImgUrl(tempArticle.imgUrl)
  }

  
  return (
    <Card sx={{
      width: "full",
      spacing: 1,
      boxShadow: 0,
      border: 1,
      borderColor: "#e0dcdc",
    }}>
      <CardMedia 
        component="img"
        height="300"
        width="100%"
        image={imgUrl}
        alt="random"
      />
      <CardContent>
        <h1>{article.title}</h1>
        <div dangerouslySetInnerHTML={{ __html: article.content }} />
      </CardContent>
    </Card>

  )
}
