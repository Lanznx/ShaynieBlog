import React from "react"
import Grid from "@mui/material/Grid"
import {
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  Typography,
} from "@mui/material"
import PropTypes from "prop-types"

ArticleCard.propTypes = {
  id: PropTypes.string,
  title: PropTypes.string,
  imgUrl: PropTypes.string,
  preview: PropTypes.string,
}

export default function ArticleCard(props) {
  const { id, title, imgUrl, preview } = props
  return (
    <Grid item xs={12} sm={6} lg={5.5} >
      <Card sx={{ width: "full", spacing: 1 }}>
        <CardActionArea onClick={() => {
          window.location.href = `/article/${id}`
        }}>
          <CardMedia
            component="img"
            height="300"
            image={imgUrl} alt="random"
          />
          <CardContent>
            <Typography gutterBottom variant="h5" component="div">
              {title}
            </Typography>
            <Typography variant="body2" color="text.secondary">
              {preview}...
            </Typography>
          </CardContent>
        </CardActionArea>
      </Card>
    </Grid>
  )
}
