import React from "react"
import Button from "@mui/material/Button"
import Grid from "@mui/material/Grid"
import { getCategories } from "../../BackStage/Editor/APIs"

export default function Categories() {
  const [categories, setCategories] = React.useState([
    { name: "全部", amount: 0 },
  ])

  const refresh = async () => {
    const tempCategories = await getCategories()
    tempCategories.unshift({ name: "全部", amount: 0 })
    setCategories(tempCategories)
  }

  React.useEffect(() => {
    refresh()
  }, [])
  return (
    <Grid container sx={{ alignContent: "space-between" }}>
      {categories.map((category) => {
        return (
          <Button color="inherit"
            onClick={() => {
              if (category === "全部") category = ""
              window.location.href = `/articles/${category.name}`
            }}
            key={category.name}
          >{category.name}</Button>
        )
      })}
    </Grid>
  )
}
