import React from "react"
import Grid from "@mui/material/Grid"
import { Typography } from "@mui/material"
import { useParams } from "react-router-dom"
import ArticleCard from "./components/ArticleCard"
import Categories from "./components/Categories"
import { getArticleListByCategory } from "./APIs"

export default function ArticleList() {
  const preloading = {
    title: "載入中",
    imgUrl: "https://firebasestorage.googleapis.com/v0/b/shaynie-" +
      "blog.appspot.com/o/article%2F%E4%B8%8A%E5%82%B3%E5%9C%96%E7" +
      "%89%87?alt=media&token=2c169b79-c802-470f-ab4b-80d6c23653b8",
    preview: "載入中",
  }
  const [articles, setArticles] = React.useState([preloading])
  const [category, setCategory] = React.useState(useParams().category)

  React.useEffect(() => {
    refresh()
    if (!category) {
      setCategory("全部")
    }
  }, [])

  const refresh = async () => {
    const temp = await getArticleListByCategory(category)
    const sortedArticleList = await quickSort(temp)
    setArticles(sortedArticleList)
  }

  const quickSort = (arr) => {
    if (arr.length <= 1) {
      return arr
    }

    const pivot = arr[0]
    const left = []
    const right = []
    for (let i = 1; i < arr.length; i++) {
      const element = arr[i]
      if (element.createTime > pivot.createTime) {
        left.push(element)
      } else {
        right.push(element)
      }
    }
    return quickSort(left).concat(pivot, quickSort(right))
  }


  return (
    <Grid container spacing={3}>
      <Grid item xs={1} lg={2}/>
      <Grid container xs={11} lg={8} spacing={3}>
        <Grid item xs={6} lg={6}>
          <Typography variant="h3" fontWeight="light" >文章</Typography>
        </Grid>
        <Grid item xs={12}>
          <Categories />
        </Grid>
        {articles.map((article) => {
          return (
            <ArticleCard
              key={article.title}
              id={article.title}
              title={article.title}
              imgUrl={article.imgUrl}
              preview={article.preview}
            />
          )
        })}
      </Grid>
      <Grid item xs={.5} lg={2} />
    </Grid>
  )
}
