import { collection, getDocs } from "firebase/firestore"
import { db } from "../../firebase/api"

const getArticleListByCategory = async (category) => {
  const articleList = []
  const path = collection(db, "article")
  const docRef = await getDocs(path)
  if (category === "全部" || category === undefined) {
    const list = []
    docRef.forEach(doc => {
      if (doc.data().visible) {
        list.push(doc.data())
      }
    })
    return list
  }

  docRef.forEach(doc => {
    if (doc.data().category === category) {
      if (doc.data().visible) {
        articleList.push(doc.data())
      }
    }
  })
  return articleList
}

export { getArticleListByCategory }