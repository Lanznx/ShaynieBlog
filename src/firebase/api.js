import { initializeApp } from "firebase/app"
import { getFirestore } from "firebase/firestore"
import { getStorage } from "firebase/storage"
import { firebaseConfig } from "./config"
const firebaseApp = initializeApp(firebaseConfig)
const storage = getStorage(firebaseApp)
const db = getFirestore(firebaseApp)

export {
  firebaseApp,
  storage,
  db,
}