import { BrowserRouter, Routes, Route } from "react-router-dom"
import React from "react"
import ReactDOM from "react-dom/client"
import Home from "./Pages/Home/HomePage"
import About from "./Pages/About/AboutPage"
import ArticleList from "./Pages/ArticleList/ArticleListPage"
import ArticlePage from "./Pages/Article/ArticlePage"
import Header from "./Pages/components/Header"
import Footer from "./Pages/components/Footer"
import { ThemeProvider, createTheme } from "@mui/material/styles"
import { Box } from "@mui/material"
import EditorPage from "./Pages/BackStage/Editor/EditorPage"
import SignIn from "./Pages/BackStage/SignIn/SignIn"
import AdminPage from "./Pages/BackStage/Admin/Admin"
import List from "./Pages/BackStage/List/List"

const root = ReactDOM.createRoot(document.getElementById("root"))
const theme = createTheme({
  palette: {
    primary: {
      main: "#fff",
      contrastText: "#000",
    },
    secondary: {
      main: "#000",
      contrastText: "#fff",
    },
  },
  typography: {
    allVariants: {
      fontFamily: [
        "sans-serif",
        // "Monospace"
      ].join(","),
    },
  },
})


root.render(
  <React.Fragment>
    <ThemeProvider theme={theme}>
      <Header />
      <Box mt={10} bgcolor="inherit">
        <BrowserRouter>
          <Routes>
            <Route path="/" exact element={<Home />} />
            <Route path="/about" element={<About />} />
            {/* <Route path="/videos" element={<Videos />} /> */}
            <Route path="/articles" element={<ArticleList />} />
            <Route path="/articles/:category" element={<ArticleList />} />
            <Route path="/article/:title" element={<ArticlePage />} />
            <Route path="/admin" element={<AdminPage />} />
            <Route path="/admin/list" element={<List />} />
            <Route path="/admin/signin" element={<SignIn />} />
            <Route path="/admin/editor" element={<EditorPage />} />
          </Routes>
        </BrowserRouter>
      </Box>

      <Footer />
    </ThemeProvider>
  </React.Fragment>,
)
